using System;
using System.Collections;
using System.Collections.Generic;
using Enemies;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Managers
{
    [Serializable]
    public struct EnemySet
    {
        public List<EnemyStats> Enemies;
    }

    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private float _defaultWaitTime = 3f;
        [SerializeField] private float _waitTimeMultiplier = 40f;
        [SerializeField] private GameObject _enemyPrefab;
        [SerializeField] private Transform _spawnPoint;
        [SerializeField] private Transform _spawnPivot;
        private EnemySet _currentEnemySet;
        private float _curseChance;
        private int _enemySetIndex = -1;
        private GameManager _gameManager;
        private StageEventManager _stageManager;
        public static EnemySpawner Instance { get; set; }

        private void Awake()
        {
            if (Instance == null) Instance = this;
        }

        private void Start()
        {
            _stageManager = StageEventManager.Instance;
            _gameManager = GameManager.Instance;
            _curseChance = 1 * _gameManager.PlayerTransform.GetComponent<PlayerController>().Stats.CurseMultiplier;
            StartCoroutine(Spawner());
            NextEnemySet();
        }

        private bool IsCursed()
        {
            return Random.Range(0f, 100f) < _curseChance;
        }

        public void NextEnemySet()
        {
            _enemySetIndex++;
            _currentEnemySet = _enemySetIndex >= _stageManager.CurrentStage.EnemySets.Length
                ? _stageManager.CurrentStage.EnemySets[_stageManager.CurrentStage.EnemySets.Length]
                : _stageManager.CurrentStage.EnemySets[_enemySetIndex];
        }

        public void SpawnEnemy()
        {
            RandomizeSpawnPivotRotation();
            var enemyGo = PoolManager.Instance.GetObjectPool("Enemy", _spawnPivot.position);
            var enemySet = _currentEnemySet;
            var enemyStats = enemySet.Enemies[Random.Range(0, enemySet.Enemies.Count)];
            var spriteRenderer = enemyGo.GetComponent<SpriteRenderer>();
            var aiController = enemyGo.GetComponent<EnemyController>();
            enemyGo.GetComponent<BoxCollider2D>().size = new Vector2(enemyStats.ColliderSize, enemyStats.ColliderSize);
            spriteRenderer.sprite = enemyStats.Sprite;
            enemyGo.transform.localScale = Vector3.one * enemyStats.Scale;
            enemyGo.name = enemyStats.Name;
            enemyGo.GetComponent<Animator>().runtimeAnimatorController = enemyStats.Controller;
            aiController.Initialize(enemyStats, IsCursed());
        }

        private void RandomizeSpawnPivotRotation()
        {
            _spawnPoint.rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
        }


        private IEnumerator Spawner()
        {
            yield return new WaitUntil(() => _stageManager.CurrentTimeSeconds > 0);
            while (true)
            {
                SpawnEnemy();
                yield return new WaitForSeconds(WaitTime(_stageManager.CurrentTimeSeconds));
            }
        }

        private float WaitTime(float time)
        {
            return Mathf.Clamp(_defaultWaitTime / time * _waitTimeMultiplier, .01f, 3f);
        }
    }
}
