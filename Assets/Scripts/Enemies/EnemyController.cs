using System;
using System.Collections;
using Interfaces;
using TMPro;
using UnityEngine;

namespace Enemies
{
    public class EnemyController : MonoBehaviour, IPooledObject, IDamageable, IKillable
    {
        [Header("Data")] [SerializeField] private Transform _destination;
        [SerializeField] private GameObject _curseFlame;

        private bool _canMove = true;
        private DamageFlash _damageFlash;
        private GameManager _gameManager;
        private Rigidbody2D _rb;
        private Transform _t;

        public EnemyStats Stats { get; set; }

        // Start is called before the first frame update
        private void Start()
        {
            _damageFlash = GetComponent<DamageFlash>();
            _rb = GetComponent<Rigidbody2D>();
            _t = transform;
            _gameManager = GameManager.Instance;
            _destination = _gameManager.PlayerTransform;
        }

        // Update is called once per frame
        private void Update()
        {
            MoveToDestination();
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            if (other.gameObject.TryGetComponent(out IDamageable damageable) &&
                other.gameObject.TryGetComponent(out PlayerController _)) damageable.Damage(Stats.FlatDamage);
        }

        public void Damage(DamageSource damageSource)
        {
            var damage = (damageSource.Perp as PlayerStats).FlatDamage + damageSource.Item.Damage;
            _damageFlash.CallDamageFlash();
            SpawnDamagePopup(damage);
            Stats.Health -= damage;
            ApplyKnockback(damageSource);
            if (Stats.Health <= 0) Die();
        }

        public void Damage(float damage)
        {

        }

        private void SpawnDamagePopup(float damage)
        {
            var popup = PoolManager.Instance.GetObjectPool("DamagePopup", new Vector2(transform.position.x,transform.position.y) + UnityEngine.Random.insideUnitCircle) ;
            var text = popup.GetComponent<TMP_Text>();
            var animator = popup.GetComponent<Animator>();
            text.SetText(((int)damage).ToString());

            animator.Play("FadeUp");

        }

        public event Action<BaseStats> OnDeath;

        public void Die()
        {
            var xp = PoolManager.Instance.GetObjectPool("Experience", _t.position);
            xp.GetComponent<Experiencer>().MaxExperience = Stats.Experience;

            OnDeath?.Invoke(Stats);
            gameObject.SetActive(false);
        }

        public void Initialize(EnemyStats stats, bool cursed)
        {
            Stats = Instantiate(stats);
            _curseFlame.SetActive(false);
            if (!cursed) return;
            var curse = Stats.Curse;
            Stats.Health *= curse.HealthMultiplier;
            Stats.FlatDamage *= curse.FlatDamageMultiplier;
            Stats.Defense *= curse.DefenseMultiplier;
            Stats.Speed *= curse.SpeedMultiplier;
            Stats.AttackSpeed *= curse.AttackSpeedMultiplier;
            Stats.Experience *= curse.ExperienceMultiplier;
            _curseFlame.SetActive(true);
        }

        private void MoveToDestination()
        {
            if (!_destination || Vector2.Distance(_t.position, _destination.position) <=
                Stats.MaxDistanceToTarget) return;
            var dir = (_destination.position - transform.position).normalized;
            Move(dir);
        }

        private void Move(Vector3 dir)
        {
            if (!_canMove) return;
            _rb.velocity = dir * Stats.Speed;
        }

        private void ApplyKnockback(DamageSource damageSource)
        {
            Stun(.33f);
            var dir = damageSource.GetComponent<Rigidbody2D>().velocity.normalized;
            var knockback = damageSource.Item.Knockback;
            _rb.AddForce(dir * knockback, ForceMode2D.Impulse);
        }

        public void Stun(float time)
        {
            if(gameObject.activeSelf)
            StartCoroutine(StunCoroutine(time));
        }

        private IEnumerator StunCoroutine(float time)
        {
            _canMove = false;
            yield return new WaitForSeconds(time);
            _canMove = true;
        }
    }
}
