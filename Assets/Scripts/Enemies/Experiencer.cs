using System.Collections;
using UnityEngine;

public class Experiencer : MonoBehaviour
{
    public ParticleSystem ExperienceParticles;
    private Leveler _leveler;
    public float MaxExperience { get; set; }


    private void Start()
    {
        var instancePlayerTransform = GameManager.Instance.PlayerTransform;
        _leveler = instancePlayerTransform.GetComponent<Leveler>();
        ExperienceParticles.trigger.AddCollider(instancePlayerTransform);
    }

    private void OnEnable()
    {
        StartCoroutine(DropExperienceCoroutine());
    }

    private void OnParticleTrigger()
    {
        _leveler.GainExperience(1f);
    }

    public IEnumerator DropExperienceCoroutine()
    {
        yield return new WaitUntil(() => MaxExperience != 0);
        var numberOfDrops = (short)Random.Range(0, MaxExperience + 1);

        var bursts = new ParticleSystem.Burst[1];
        bursts[0] = new ParticleSystem.Burst(0, numberOfDrops);
        ExperienceParticles.emission.SetBursts(bursts, 1);

        ExperienceParticles.Play();
    }
}
