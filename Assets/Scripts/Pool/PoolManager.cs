using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Pool
{
    public string Tag;
    public GameObject PoolPrefab;
    public Transform Parent;
    public int Size;
}

public class PoolManager : MonoBehaviour
{
    private static PoolManager _instance;

    [SerializeField] private Pool[] _pools;
    private Dictionary<string, Queue<GameObject>> _objectPools;

    public static PoolManager Instance
    {
        get
        {
            if (!_instance) Debug.LogError("Pool Manager instance is null.");
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        _objectPools = new Dictionary<string, Queue<GameObject>>();
        foreach (var pool in _pools)
        {
            var objectPool = new Queue<GameObject>();
            for (var i = 0; i < pool.Size; i++)
            {
                var obj = Instantiate(pool.PoolPrefab);
                if (pool.Parent) obj.transform.parent = pool.Parent;
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            _objectPools.Add(pool.Tag, objectPool);
        }
    }

    public GameObject GetObjectPool(string tag, Vector2 position)
    {
        var obj = _objectPools[tag].Dequeue();
        obj.SetActive(true);
        _objectPools[tag].Enqueue(obj);
        obj.transform.position = position;
        return obj;
    }

    public Queue<GameObject> GetPool(string tag)
    {
        return _objectPools[tag];
    }
}
