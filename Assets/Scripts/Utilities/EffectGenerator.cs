using DG.Tweening;
using UnityEngine;

public class EffectGenerator
{
    private readonly Color _originalColor;
    private readonly SpriteRenderer _spriteRenderer;
    private readonly GameObject _target;
    private Tween _tweenColorReference;

    public EffectGenerator(GameObject target)
    {
        _target = target;
        _spriteRenderer = _target.GetComponent<SpriteRenderer>();
        _originalColor = _spriteRenderer.color;
    }

    public void Hit(float duration)
    {
        if (_tweenColorReference != null && _tweenColorReference.active) _tweenColorReference.Kill();
        _spriteRenderer.color = Color.white;
        _tweenColorReference = _spriteRenderer.DOColor(_originalColor, duration)
            .SetEase(Ease.OutCubic)
            .OnComplete(() => _tweenColorReference = null);
    }
}
