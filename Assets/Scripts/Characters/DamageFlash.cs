using System.Collections;
using UnityEngine;

public class DamageFlash : MonoBehaviour
{
    [SerializeField] private Color _flashColor = Color.white;

    [SerializeField] private float _flashTime = .5f;
    private Coroutine _coroutine;
    private Material _material;

    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _material = _spriteRenderer.material;
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void CallDamageFlash()
    {
        if (!gameObject.activeSelf) return;
        _coroutine = StartCoroutine(DamageFlashCoroutine());
    }

    private IEnumerator DamageFlashCoroutine()
    {
        _material.SetColor("_FlashColor", _flashColor);

        var currentFlashAmount = 0f;
        var elapsedTime = 0f;
        while (elapsedTime < _flashTime)
        {
            elapsedTime += Time.deltaTime;
            currentFlashAmount = Mathf.Lerp(1f, 0f, elapsedTime / _flashTime);
            _material.SetFloat("_FlashAmount", currentFlashAmount);
            yield return null;
        }
    }
}
