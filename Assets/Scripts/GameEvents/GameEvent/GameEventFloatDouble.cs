using UnityEngine;

[CreateAssetMenu(menuName = "GameEvents/GameEventFloatDouble")]
public class GameEventFloatDouble : GameEvent<float, float>
{
}
