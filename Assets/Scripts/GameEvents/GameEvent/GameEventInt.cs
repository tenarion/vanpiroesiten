using UnityEngine;

[CreateAssetMenu(menuName = "GameEvents/GameEventInt")]
public class GameEventInt : GameEvent<int>
{
}
