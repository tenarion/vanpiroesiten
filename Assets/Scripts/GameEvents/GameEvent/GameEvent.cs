﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

public abstract class GameEvent<T> : ScriptableObject
{
    private readonly List<GameEventListener<T>> _eventListeners = new();

    public void Raise(T parameter)
    {
        for (var i = _eventListeners.Count - 1; i >= 0; i--)
            _eventListeners[i].OnEventRaised(parameter);
    }

    public void RegisterListener(GameEventListener<T> listener)
    {
        if (!_eventListeners.Contains(listener))
            _eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListener<T> listener)
    {
        if (_eventListeners.Contains(listener))
            _eventListeners.Remove(listener);
    }
}

public abstract class GameEvent<T1, T2> : ScriptableObject
{
    private readonly List<GameEventListener<T1, T2>> _eventListeners = new();

    public void Raise(T1 parameter1, T2 parameter2)
    {
        for (var i = _eventListeners.Count - 1; i >= 0; i--)
            _eventListeners[i].OnEventRaised(parameter1, parameter2);
    }

    public void RegisterListener(GameEventListener<T1, T2> listener)
    {
        if (!_eventListeners.Contains(listener))
            _eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListener<T1, T2> listener)
    {
        if (_eventListeners.Contains(listener))
            _eventListeners.Remove(listener);
    }
}

public class GameEvent : ScriptableObject
{
    private readonly List<GameEventListener> _eventListeners = new();

    public void Raise()
    {
        for (var i = _eventListeners.Count - 1; i >= 0; i--)
            _eventListeners[i].OnEventRaised();
    }

    public void RegisterListener(GameEventListener listener)
    {
        if (!_eventListeners.Contains(listener))
            _eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListener listener)
    {
        if (_eventListeners.Contains(listener))
            _eventListeners.Remove(listener);
    }
}
