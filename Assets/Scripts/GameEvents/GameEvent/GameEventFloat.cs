using UnityEngine;

[CreateAssetMenu(menuName = "GameEvents/GameEventFloat")]
public class GameEventFloat : GameEvent<float>
{
}
