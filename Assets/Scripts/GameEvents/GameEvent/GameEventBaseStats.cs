using UnityEngine;

[CreateAssetMenu(menuName = "GameEvents/GameEventBaseStats")]
public class GameEventBaseStats : GameEvent<BaseStats>
{
}
