﻿using UnityEngine;
using UnityEngine.Events;

public abstract class GameEventListener<T> : MonoBehaviour
{
    public GameEvent<T> Event;
    public UnityEvent<T> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(T parameter)
    {
        Response.Invoke(parameter);
    }
}

public abstract class GameEventListener<T1, T2> : MonoBehaviour
{
    public GameEvent<T1, T2> Event;
    public UnityEvent<T1, T2> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(T1 parameter1, T2 parameter2)
    {
        Response.Invoke(parameter1, parameter2);
    }
}

public class GameEventListener : MonoBehaviour
{
    public UnityEvent Response;
    public GameEvent Event;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
        Response.Invoke();
    }
}
