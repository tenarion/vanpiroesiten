public interface IDamageable
{
    public void Damage(DamageSource damageSource);
    public void Damage(float damage);
}
