﻿using System;

namespace Interfaces
{
    public interface IKillable
    {
        public event Action<BaseStats> OnDeath;
        public void Die();
    }
}
