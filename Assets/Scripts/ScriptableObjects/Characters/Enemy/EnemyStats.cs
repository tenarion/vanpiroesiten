using System;
using ScriptableObjects.Characters;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Characters/Enemy")]
public class EnemyStats : CharacterStats
{
    public float Scale = 1;
    public float MaxDistanceToTarget;
    public float Experience;
    public float ColliderSize = 1;

    [Header("Curse Properties")] public Curse Curse;
}

[Serializable]
public struct Curse
{
    [Range(1, 5)] public float HealthMultiplier;
    [Range(1, 5)] public float SpeedMultiplier;
    [Range(1, 5)] public float AttackSpeedMultiplier;
    [Range(1, 5)] public float DefenseMultiplier;
    [Range(1, 5)] public float FlatDamageMultiplier;
    [Range(1, 5)] public float ExperienceMultiplier;
}
