using ScriptableObjects.Characters;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player", menuName = "Characters/Player")]
public class PlayerStats : CharacterStats
{
    [Tooltip("Multiplier to the chance of obtaining a chest/better item/random pickups on the scene.")]
    [Range(0, 10)]
    public float LuckMultiplier;

    [Tooltip("Multiplier to the chance of cursed enemies to spawn.")]
    [Range(0, 10)]
    public float CurseMultiplier;

    [Tooltip("Regeneration of health per second.")]
    [Range(0, 2)]
    public float HealthRegen;

    public List<ActiveItem> StartingActiveItems;
    public List<PassiveItem> StartingPassiveItems;
}