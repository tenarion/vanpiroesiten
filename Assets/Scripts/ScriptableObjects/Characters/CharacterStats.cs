using UnityEditor.Animations;
using UnityEngine;

namespace ScriptableObjects.Characters
{
    public abstract class CharacterStats : BaseStats
    {
        public AnimatorController Controller;
        [Header("Stats Properties")] public float Health;

        public float Speed;
        public float AttackSpeed;
        public float Defense;
        public float FlatDamage;
    }
}
