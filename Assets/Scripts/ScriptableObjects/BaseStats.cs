using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseStats : ScriptableObject
{
    [Header("Display Properties")]

    public Sprite Sprite;
    public string Name;
    public string Description;
}
