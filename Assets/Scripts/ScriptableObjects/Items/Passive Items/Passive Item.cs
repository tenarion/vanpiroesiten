using UnityEngine;

[CreateAssetMenu(menuName = "Items/Passive Items/New Passive Item", fileName = "New Passive Item")]
public class PassiveItem : BaseStats
{
    public ActiveItemType ActiveItemType;
    public float AttackSpeedMultiplier = 1;
    public float AttackTimeMultiplier = 1;
    public float DamageMultiplier = 1;
    public float RangeMultiplier = 1;
    public float KnockbackMultiplier = 1;
    public float ReloadTimeMultiplier = 1;
}
