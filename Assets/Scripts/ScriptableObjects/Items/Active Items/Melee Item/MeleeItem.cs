using UnityEngine;

[CreateAssetMenu(menuName = "Items/Active Items/Melee Item", fileName = "New Melee Item")]
public class MeleeItem : ActiveItem
{
    [Header("Unique Stats")] public float AttackRange;

    public Vector2[] AttackDirections; //for each direction is a "melee attack"
    public Pattern Pattern;
}
