using UnityEngine;

public abstract class ActiveItem : BaseStats
{
    [Header("Item Stats")] [ReadOnly] public ActiveItemType Type;

    public Sprite AttackSprite;
    [SerializeField] private float _attackTime;
    public float Damage;
    public float MaxRange;
    public float ReloadTime;
    public int Magazine;
    public float Knockback;

    private float a = 1;

    [HideInInspector]
    public float AttackTime
    {
        get => a / _attackTime;
        set => a = value;
    }

    private void OnValidate()
    {
        if (GetType() == typeof(ProjectileItem)) Type = ActiveItemType.PROJECTILE;
        else if (GetType() == typeof(MeleeItem)) Type = ActiveItemType.MELEE;
        else if (GetType() == typeof(AuraItem)) Type = ActiveItemType.AURA;
    }
}
