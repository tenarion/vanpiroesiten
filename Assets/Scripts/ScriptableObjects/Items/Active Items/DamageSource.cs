using Enemies;
using UnityEngine;

public class DamageSource : MonoBehaviour
{
    private Vector3 startPosition;
    public BaseStats Perp { get; set; }
    public ActiveItem Item { get; set; }
    public bool IsPlayer => Perp is PlayerStats;

    private void Start()
    {
        startPosition = transform.position;
    }

    private void Update()
    {
        var dist = (transform.position - startPosition).magnitude;
        if (dist >= Item.MaxRange) gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.TryGetComponent(out IDamageable damageable) ||
            (collision.TryGetComponent(out PlayerController _) && IsPlayer) ||
            (collision.TryGetComponent(out EnemyController _) && !IsPlayer)) return;

        damageable.Damage(this);
        gameObject.SetActive(false);
    }
}
