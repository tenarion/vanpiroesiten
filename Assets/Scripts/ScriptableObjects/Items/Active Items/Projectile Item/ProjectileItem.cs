using UnityEngine;

[CreateAssetMenu(menuName = "Items/Active Items/Projectile Item", fileName = "New Projectile Item")]
public class ProjectileItem : ActiveItem
{
    [Header("Unique Stats")] public float AttackSpeed;

    public int Piercing = 1;
    public float Scale = 1;
    public Color Color = Color.white;
    public Pattern Pattern;
}
