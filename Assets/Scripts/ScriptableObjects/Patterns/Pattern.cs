using UnityEngine;

[CreateAssetMenu(fileName = "New Pattern", menuName = "Patterns/New Pattern")]
public class Pattern : ScriptableObject
{
    public int ProjectileSize;
    public float AngleOffset;
}
