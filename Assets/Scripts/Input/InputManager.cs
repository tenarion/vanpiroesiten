using UnityEngine;

namespace Input
{
    public class InputManager : MonoBehaviour
    {
        private static InputManager _instance;
        public static InputManager Instance => _instance;
        public GameActions Actions;

        private void Awake()
        {
            if (_instance != null && _instance != this) Destroy(this);
            else _instance = this;
        }

        private void OnEnable()
        {
            Actions = new GameActions();
            Actions.Enable();
        }

        private void OnDisable()
        {
            Actions.Disable();
        }
    }
}
