using System;
using System.Collections.Generic;
using UnityEngine.Events;

[Serializable]
public struct StageEvent
{
    public float TimeStamp;
    public UnityEvent Event;
}
