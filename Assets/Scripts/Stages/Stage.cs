using System;
using System.Collections.Generic;
using Managers;

[Serializable]
public class Stage
{
    public List<StageEvent> Events;
    public List<BaseStats> AvailableItems;
    public EnemySet[] EnemySets = new EnemySet[5];
}
