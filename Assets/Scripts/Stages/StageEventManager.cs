using System;
using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;

public class StageEventManager : MonoBehaviour
{
    public int CurrentTimeSeconds;
    [SerializeField] private List<Stage> _stages;
    private readonly WaitForSeconds _wfs = new(1);

    private EnemySpawner _enemySpawner;
    private int _eventIndex;
    public Stage CurrentStage { get; private set; }
    public static StageEventManager Instance { get; private set; }
    public int CurrentStageIndex { get; }
    public float CurrentTimeMins => CurrentTimeSeconds / 60f;

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    private void Start()
    {
        _enemySpawner = EnemySpawner.Instance;
        StartTimer();
    }

    private void Update()
    {
    }

    public event Action<int> OnTime;

    public void StartTimer()
    {
        StartCoroutine(StartTimerCorroutine());
    }

    private IEnumerator StartTimerCorroutine()
    {
        CurrentStage = _stages[CurrentStageIndex];
        CurrentTimeSeconds = 0;
        while (true)
        {
            if (_eventIndex < CurrentStage.Events.Count &&
                CurrentStage.Events[_eventIndex].TimeStamp == CurrentTimeSeconds)
            {
                CurrentStage.Events[_eventIndex].Event.Invoke();
                _eventIndex++;
            }

            CurrentTimeSeconds++;
            OnTime?.Invoke(CurrentTimeSeconds);
            yield return _wfs;
        }
    }

    public List<BaseStats> GetAvailableItems()
    {
        var items = CurrentStage.AvailableItems;
        return items;
    }

    public void SpawnBoss()
    {
    }

    public void EnemyAmbush(int enemyQuantity)
    {
        for (var i = 0; i < enemyQuantity; i++) _enemySpawner.SpawnEnemy();
    }
}
