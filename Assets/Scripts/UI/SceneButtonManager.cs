using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneButtonManager : MonoBehaviour
{
    private static SceneButtonManager _instance;
    public static SceneButtonManager Instance { get; private set; }


    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    public void LoadGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
