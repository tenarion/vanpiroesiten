using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemButton : MonoBehaviour
{
    [SerializeField] private Button _button;
    public GameEventBaseStats OnAddItem;
    public TMP_Text _text;
    private BaseStats _item;
    private StageEventManager _stageEventManager;

    private void Start()
    {
        _stageEventManager = StageEventManager.Instance;
    }

    private void OnEnable()
    {
        GetRandomItem();
    }

    public void AddItem()
    {
        OnAddItem.Raise(_item);
    }

    private void GetRandomItem()
    {
        _stageEventManager = StageEventManager.Instance;
        var items = _stageEventManager.GetAvailableItems();
        _item = items[Random.Range(0, items.Count)];
        _text.SetText(_item.Name);
    }
}
