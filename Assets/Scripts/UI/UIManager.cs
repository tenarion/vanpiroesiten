using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Slider _experienceBar;
    [SerializeField] private TMP_Text _levelText;
    [SerializeField] private TMP_Text _timeText;
    [SerializeField] private RectTransform _selectItemPanel;
    [SerializeField] private Slider _lifeBar;
    private StageEventManager _stageEventManager;

    // Start is called before the first frame update
    private void Start()
    {
        _stageEventManager = StageEventManager.Instance;
        _stageEventManager.OnTime += SetTimeText;
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void ToggleSelectItemPanel(bool state)
    {
        if (state) Time.timeScale = 0;
        else Time.timeScale = 1;
        _selectItemPanel.gameObject.SetActive(state);
    }

    public void SetExperienceBar(float maxExperience, float currentExperience)
    {
        _experienceBar.value = currentExperience / maxExperience;
    }

    public void SetLevelText(int level)
    {
        _levelText.SetText($"LVL: {level}");
    }

    public void SetLifeBar(float currentHealth, float maxHealth)
    {
        _lifeBar.value = currentHealth / maxHealth;
    }

    public void SetTimeText(int timeSeconds)
    {
        var minutes = timeSeconds / 60;
        var seconds = timeSeconds % 60;
        _timeText.SetText($"{(minutes <= 9 ? "0" + minutes : minutes)}:{(seconds <= 9 ? "0" + seconds : seconds)}");
    }
}
