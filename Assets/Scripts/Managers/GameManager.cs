using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private readonly WaitForSeconds _wfs = new(1);
    public static GameManager Instance { get; private set; }



    [Header("World")]
    public Transform WorldWrapper;
    public Transform ManagementWrapper;
    public Transform EnemiesWrapper;
    public Transform BulletWrapper;

    public Transform PlayerTransform;

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }


    private void Start()
    {

    }
}
