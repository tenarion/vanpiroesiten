using Enemies;
using UnityEngine;

public class Leveler : MonoBehaviour
{
    public int Level = 1;
    public float CurrentExperience;
    [ReadOnly] public float MaxExperience;

    public GameEventInt OnLevelUp;
    public GameEventFloatDouble OnExperienceGained;

    [Header("Experience Parameters")] [SerializeField]
    public float ScalingFactor;

    [SerializeField] public float GrowthFactor;
    private float _bufferExperience;

    private void Start()
    {
        foreach (var enemy in PoolManager.Instance.GetPool("Enemy"))
        {
            var controller = enemy.GetComponent<EnemyController>();
            controller.OnDeath += GainExperience;
        }
    }

    private void OnValidate()
    {
        if (CurrentExperience >= MaxExperience) LevelUp();
        if (Level <= 0) Level = 1;
        MaxExperience = ScalingFactor * Mathf.Pow(GrowthFactor, Level);
    }

    public void LevelUp()
    {
        Level++;
        OnLevelUp.Raise(Level);
        CurrentExperience = _bufferExperience;
        MaxExperience = ScalingFactor * Mathf.Pow(GrowthFactor, Level);
        var differenceExperience = CurrentExperience - MaxExperience;
        if (!(differenceExperience >= 0)) return;
        _bufferExperience = differenceExperience;
        LevelUp();
    }

    public void GainExperience(BaseStats stats)
    {
        if (stats is not EnemyStats enemyStats) return;
        GainExperience(enemyStats.Experience);
    }

    public void GainExperience(float experience)
    {
        CurrentExperience += experience;
        var differenceExperience = CurrentExperience - MaxExperience;
        OnExperienceGained.Raise(MaxExperience, differenceExperience >= 0 ? differenceExperience : CurrentExperience);
        if (!(differenceExperience >= 0)) return;
        _bufferExperience = differenceExperience;
        LevelUp();
    }
}
