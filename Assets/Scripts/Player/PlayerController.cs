using System;
using System.Collections;
using System.Linq;
using Input;
using Interfaces;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour, IDamageable, IKillable
{
    [SerializeField] private PlayerStats _templateStats;
    [HideInInspector] public PlayerStats Stats;
    public GameEventFloatDouble OnDamageReceived;
    private GameActions _actions;
    private EffectGenerator _effectGenerator;
    private bool _isInmortal;
    private float _maxHealth;

    // Start is called before the first frame update
    private void Start()
    {
        _effectGenerator = new EffectGenerator(gameObject);
        _actions = InputManager.Instance.Actions;
        Stats = Instantiate(_templateStats);
        _maxHealth = Stats.Health;
        StartCoroutine(RegenerateHealthCoroutine());
    }

    // Update is called once per frame
    private void Update()
    {
        Move();
    }

    public void Damage(DamageSource damageSource)
    {
        if (_isInmortal) return;
        StartCoroutine(DamageCooldownCoroutine());
        _effectGenerator.Hit(.5f);
    }

    public void Damage(float damage)
    {
        if (_isInmortal) return;
        StartCoroutine(DamageCooldownCoroutine());
        _effectGenerator.Hit(.5f);
        Stats.Health -= damage;
        OnDamageReceived.Raise(Stats.Health, _maxHealth);
        if (Stats.Health <= 0) Die();
    }

    public event Action<BaseStats> OnDeath;

    public void Die()
    {
        OnDeath?.Invoke(Stats);
        SceneManager.LoadScene("DeathScene");
    }

    private void Move()
    {
        var dir = _actions.Player.Move.ReadValue<Vector2>();
        var movement = (Vector3)dir * (Stats.Speed * Time.deltaTime);
        transform.position += IsSurrounded() ? movement / 2 : movement;
    }

    private bool IsSurrounded()
    {
        var t = transform;
        return Physics2D.CircleCastAll(t.position, 1f, t.forward)
            .Where(hit => hit.transform.CompareTag("Enemy"))
            .ToList()
            .Count > 10;
    }

    private IEnumerator DamageCooldownCoroutine()
    {
        _isInmortal = true;
        yield return new WaitForSeconds(.5f);
        _isInmortal = false;
    }

    private IEnumerator RegenerateHealthCoroutine()
    {
        while (true)
        {
            if (Stats.Health < _maxHealth)
            {
                Stats.Health += Stats.HealthRegen;
                OnDamageReceived.Raise(Stats.Health, _maxHealth);
            }

            yield return new WaitForSeconds(1f);
        }
    }
}
