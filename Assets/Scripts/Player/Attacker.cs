using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour
{
    [SerializeField] private PlayerStats _templateStats;
    private List<ActiveItem> _activeItems;
    private List<PassiveItem> _passiveItems;

    private PoolManager _poolManager;

    // Start is called before the first frame update
    private void Start()
    {
        _activeItems = new List<ActiveItem>();
        _passiveItems = new List<PassiveItem>();
        _poolManager = PoolManager.Instance;
        foreach (var item in _templateStats.StartingActiveItems) AddItem(item);
        foreach (var item in _templateStats.StartingPassiveItems) _passiveItems.Add(item);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public event Action OnItemAdded;

    private void Attack(ActiveItem item)
    {
        switch (item.Type)
        {
            case ActiveItemType.MELEE:
            case ActiveItemType.PROJECTILE:
                EvaluatePattern(item);
                break;
            case ActiveItemType.AURA:

                break;
        }
    }

    public void EvaluatePattern(ActiveItem item)
    {
        if (item is ProjectileItem projectile)
        {
            var projectileSize = projectile.Pattern.ProjectileSize;
            var angleOffset = projectile.Pattern.AngleOffset;
            var angularSpacing = 360f / projectileSize;
            var currentAngle = angleOffset;
            for (var i = 0; i < projectileSize; i++)
            {
                var itemGo = _poolManager.GetObjectPool("Item", transform.position);
                var spriteRenderer = itemGo.GetComponent<SpriteRenderer>();
                spriteRenderer.sprite = projectile.AttackSprite;
                spriteRenderer.color = projectile.Color;
                itemGo.transform.localScale = Vector3.one * projectile.Scale;
                var vel = projectile.AttackSpeed *
                          (Mathf.Abs(Mathf.Sin(projectileSize * currentAngle * Mathf.Deg2Rad)) + 1);
                var velVector = new Vector2(Mathf.Cos(Mathf.Deg2Rad * currentAngle),
                    Mathf.Sin(Mathf.Deg2Rad * currentAngle)) * vel;
                itemGo.GetComponent<Rigidbody2D>().velocity = velVector;
                var dmgSource = itemGo.GetComponent<DamageSource>();
                dmgSource.Perp = _templateStats;
                dmgSource.Item = item;
                currentAngle += angularSpacing;
            }
        }
        //stuff
    }

    public void AddItem(BaseStats item)
    {
        var newItem = Instantiate(item);
        switch (newItem)
        {
            case PassiveItem passive:
                _passiveItems.Add(passive);
                ManagePassive(passive);
                break;
            case ActiveItem active:
                _activeItems.Add(active);
                ApplyPassives(active);
                StartCoroutine(StartItemCoroutine(active));
                break;
        }

        OnItemAdded?.Invoke();
    }

    private void ApplyPassives(ActiveItem item)
    {
        foreach (var passive in _passiveItems) ManagePassive(passive, item);
    }

    private void ManagePassive(PassiveItem passive)
    {
        _passiveItems.Add(passive);
        foreach (var item in _activeItems) ManagePassive(passive, item);
    }

    private void ManagePassive(PassiveItem passive, ActiveItem active)
    {
        var type = passive.ActiveItemType;
        switch (type)
        {
            case ActiveItemType.MELEE:
                var melee = active as MeleeItem;
                break;
            case ActiveItemType.PROJECTILE:
                var projectile = active as ProjectileItem;
                projectile.AttackSpeed *= passive.AttackSpeedMultiplier;
                //projectile.AttackTime /= passive.AttackTimeMultiplier;
                projectile.Damage *= passive.DamageMultiplier;
                projectile.Knockback *= passive.KnockbackMultiplier;
                projectile.ReloadTime /= passive.ReloadTimeMultiplier;
                projectile.MaxRange *= passive.RangeMultiplier;
                break;
            case ActiveItemType.AURA:
                var aura = active as AuraItem;
                break;
        }
    }

    private IEnumerator StartItemCoroutine(ActiveItem item)
    {
        yield return new WaitForSeconds(1);
        while (true)
        {
            for (var i = 0; i < item.Magazine; i++)
            {
                Attack(item);
                print(item.Name + " " + item.ReloadTime);
                yield return new WaitForSeconds(item.AttackTime);
            }
            yield return new WaitForSeconds(item.ReloadTime);
        }
    }
}
